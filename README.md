# TrainsCompagny

TrainsCompagny

## Getting started

Le programme prend deux paramètres en entrée :
 - le premier est le chemin du fichier (.json) d'entrée (la lite des taps)
 - le deuxième est le chemin du fichier de sortie.

Le programme s'exécute avec la commande : 
java -jar TrainsCompagny.jar <CHEMIN_FICHIER_ENTREE> <CHEMIN_FICHIER_SORTE>

L'exécutable 'executeTask.bat' permet de lancer le programme avec le fichier d'entrée fourni 'CandidateInputExample.json' et génère un fichier 'outPut.json' en sortie