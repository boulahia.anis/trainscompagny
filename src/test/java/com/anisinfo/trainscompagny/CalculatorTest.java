/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anisinfo.trainscompagny;

import com.anisinfo.trainscompagny.models.Trip;
import com.anisinfo.trainscompagny.services.Calculator;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

/**
 *
 * @author boulahia
 */
public class CalculatorTest {

    @Test
    public void should_return_240_within_zone1_And_zone2_Test() {
        Trip trip = new Trip("A", "B");
        Calculator.calclulatePrice(trip);
        assertEquals(240, trip.getCostInCents());

        trip = new Trip("A", "D");
        Calculator.calclulatePrice(trip);
        assertEquals(240, trip.getCostInCents());

        trip = new Trip("C", "D");
        Calculator.calclulatePrice(trip);
        assertEquals(240, trip.getCostInCents());

        trip = new Trip("C", "A");
        Calculator.calclulatePrice(trip);
        assertEquals(240, trip.getCostInCents());
    }

    @Test
    public void should_return_200_within_zone3_And_zone4_Test() {
        Trip trip = new Trip("C", "F");
        Calculator.calclulatePrice(trip);
        assertEquals(200, trip.getCostInCents());

        trip = new Trip("E", "H");
        Calculator.calclulatePrice(trip);
        assertEquals(200, trip.getCostInCents());

        trip = new Trip("I", "C");
        Calculator.calclulatePrice(trip);
        assertEquals(200, trip.getCostInCents());

        trip = new Trip("F", "I");
        Calculator.calclulatePrice(trip);
        assertEquals(200, trip.getCostInCents());

    }

    @Test
    public void within_zone3_And_zone1_2_Test() {
        Trip trip = new Trip("C", "D");
        Calculator.calclulatePrice(trip);
        assertEquals(240, trip.getCostInCents());

        trip = new Trip("E", "A");
        Calculator.calclulatePrice(trip);
        assertEquals(240, trip.getCostInCents());

        trip = new Trip("A", "E");
        Calculator.calclulatePrice(trip);
        assertEquals(240, trip.getCostInCents());

        trip = new Trip("F", "A");
        Calculator.calclulatePrice(trip);
        assertEquals(280, trip.getCostInCents());
    }

    @Test
    public void within_zone4_And_zone1_2_Test() {
        Trip trip = new Trip("F", "E");
        Calculator.calclulatePrice(trip);
        assertEquals(200, trip.getCostInCents());

        trip = new Trip("F", "B");
        Calculator.calclulatePrice(trip);
        assertEquals(280, trip.getCostInCents());

        trip = new Trip("G", "B");
        Calculator.calclulatePrice(trip);
        assertEquals(300, trip.getCostInCents());

        trip = new Trip("I", "A");
        Calculator.calclulatePrice(trip);
        assertEquals(300, trip.getCostInCents());

        trip = new Trip("A", "I");
        Calculator.calclulatePrice(trip);
        assertEquals(300, trip.getCostInCents());
    }
}
