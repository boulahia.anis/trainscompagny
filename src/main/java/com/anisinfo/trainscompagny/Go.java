/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anisinfo.trainscompagny;

import com.anisinfo.trainscompagny.models.Customer;
import com.anisinfo.trainscompagny.models.*;
import com.anisinfo.trainscompagny.services.Calculator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author boulahia
 */
public class Go {

    public static final List<Customer> CUSTOMERS = new ArrayList<>();

    public static void main(String[] args) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            Taps taps = mapper.readValue(new File(args[0]), Taps.class);


            // trier les taps par date asc
            // sort taps by date asc
            taps.getTaps().sort(Comparator.comparing(Tap::getUnixTimestamp));

            // parcourir les taps pour reconstruire les trajets de chaque client
            // iterate through the taps to reconstruct the journeys of each customer
            taps.getTaps().forEach(tap -> {
                Optional<Customer> optionalCus = CUSTOMERS.stream().filter(cus -> cus.getCustomerId() == tap.getCustomerId()).findFirst();
                Customer customer = null;
                // recupérer le client si existe
                // get the customer if exists
                if (optionalCus.isPresent()) {
                    customer = optionalCus.get();
                } else {
                    // sinon créer le client 
                    // else create new customer
                    customer = new Customer();
                    customer.setCustomerId(tap.getCustomerId());
                    customer.setTrips(new ArrayList<>());
                    CUSTOMERS.add(customer);
                }

                // initialiser le premier trip (trajet aller)
                // initialize the first trip (outward trip)
                if (customer.getTrips().isEmpty()) {
                    customer.getTrips().add(initTrip(tap));
                } else {
                    Trip trip = customer.getTrips().get(customer.getTrips().size() - 1);
                    // si chemin retour compléter les infos du trajet
                    // if way back complete the trip info
                    if (trip.getStationEnd() == null) {
                        trip.setStationEnd(tap.getStation());
                        Calculator.calclulatePrice(trip);
                        customer.setTotalCostInCents(customer.getTotalCostInCents() + trip.getCostInCents());
                    } 
                    // sinon inititaliser un nouveau trajet
                    // otherwise initialize a new path
                    else {
                        customer.getTrips().add(initTrip(tap));
                    }
                }
            });

            // créer le fichier json de sortie
            // create the output json file
            OutPut output = new OutPut();
            output.setCustomerSummaries(CUSTOMERS);
            mapper.writeValue(new File(args[1]), output);

        } catch (JsonProcessingException ex) {
            Logger.getLogger(Go.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Go.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * return an initialized Trip with : StationStart, StartedJourneyAt
     *
     * @param tap
     * @return
     */
    private static Trip initTrip(Tap tap) {
        Trip trip = new Trip();
        trip.setStationStart(tap.getStation());
        trip.setStartedJourneyAt(tap.getUnixTimestamp());
        return trip;
    }

}
