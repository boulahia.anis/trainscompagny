/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anisinfo.trainscompagny.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author boulahia
 */
public class Taps {

    @JsonProperty("taps")
    private List<Tap> taps = new ArrayList<>();

    public Taps() {

    }

    public List<Tap> getTaps() {
        return taps;
    }

    public void setTaps(List<Tap> taps) {
        this.taps = taps;
    }

    @Override
    public String toString() {
        return "Taps{" + "taps=" + taps + '}';
    }
    
    

}
