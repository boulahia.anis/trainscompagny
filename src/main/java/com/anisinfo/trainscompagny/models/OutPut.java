/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anisinfo.trainscompagny.models;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author boulahia
 */
public class OutPut {
    
    private List<Customer> customerSummaries = new ArrayList<>();
    
    public OutPut() {
    }

    public List<Customer> getCustomerSummaries() {
        return customerSummaries;
    }

    public void setCustomerSummaries(List<Customer> customerSummaries) {
        this.customerSummaries = customerSummaries;
    }
    
    
}
