/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anisinfo.trainscompagny.models;

import java.util.List;

/**
 *
 * @author boulahia
 */
public class Customer {
    
    private int customerId;
    private int totalCostInCents;
    private List<Trip> trips;

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public int getTotalCostInCents() {
        return totalCostInCents;
    }

    public void setTotalCostInCents(int totalCostInCents) {
        this.totalCostInCents = totalCostInCents;
    }

    public List<Trip> getTrips() {
        return trips;
    }

    public void setTrips(List<Trip> trips) {
        this.trips = trips;
    }

    @Override
    public String toString() {
        return "Customer{" + "customerId=" + customerId + ", totalCostInCents=" + totalCostInCents + ", trips=" + trips + '}';
    }
    
    
}
