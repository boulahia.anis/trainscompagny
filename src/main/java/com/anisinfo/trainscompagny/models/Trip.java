/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anisinfo.trainscompagny.models;

/**
 *
 * @author boulahia
 */
public class Trip {
    
    private String stationStart;
    private String stationEnd;
    private int startedJourneyAt;
    private int costInCents;
    private int zoneFrom;
    private int zoneTo;

    public Trip(){
        
    }
    
    public Trip(String stationStart, String stationEnd) {
        this.stationStart = stationStart;
        this.stationEnd = stationEnd;
    }

    public String getStationStart() {
        return stationStart;
    }

    public void setStationStart(String stationStart) {
        this.stationStart = stationStart;
    }

    public String getStationEnd() {
        return stationEnd;
    }

    public void setStationEnd(String stationEnd) {
        this.stationEnd = stationEnd;
    }

    public int getStartedJourneyAt() {
        return startedJourneyAt;
    }

    public void setStartedJourneyAt(int startedJourneyAt) {
        this.startedJourneyAt = startedJourneyAt;
    }

    public int getCostInCents() {
        return costInCents;
    }

    public void setCostInCents(int costInCents) {
        this.costInCents = costInCents;
    }

    public int getZoneFrom() {
        return zoneFrom;
    }

    public void setZoneFrom(int zoneFrom) {
        this.zoneFrom = zoneFrom;
    }

    public int getZoneTo() {
        return zoneTo;
    }

    public void setZoneTo(int zoneTo) {
        this.zoneTo = zoneTo;
    }

    @Override
    public String toString() {
        return "Trip{" + "stationStart=" + stationStart + ", stationEnd=" + stationEnd + ", startedJourneyAt=" + startedJourneyAt + ", costInCents=" + costInCents + ", zoneFrom=" + zoneFrom + ", zoneTo=" + zoneTo + '}';
    }
    
    
}
