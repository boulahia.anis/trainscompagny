/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anisinfo.trainscompagny.services;

import com.anisinfo.trainscompagny.models.Trip;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author boulahia
 */
public class Calculator {

    public static final String ZONE_1 = "A,B";
    public static final String ZONE_2 = "C,D,E";
    public static final String ZONE_3 = "C,E,F";
    public static final String ZONE_4 = "F,G,H,I";

    public static final int[][] PRICES = {
        {240, 240, 280, 300},
        {240, 240, 280, 300},
        {280, 280, 200, 200},
        {300, 300, 200, 200}
    };

    /**
     * CalculPrice, ZoneFrom, ZoneTo
     * @param trip 
     */
    public static void calclulatePrice(Trip trip) {
        List<Integer> startZones = getZones(trip.getStationStart());
        List<Integer> endZones = getZones(trip.getStationEnd());
        int price = 0;
        for (Integer start : startZones) {
            for (Integer end : endZones) {
                if (price == 0 || PRICES[start - 1][end - 1] < price ) {
                    price = PRICES[start - 1][end - 1];
                    trip.setZoneFrom(start);
                    trip.setZoneTo(end);
                    trip.setCostInCents(price);
                }
            }
        }
    }

    /**
     * get Zone/s from station
     * @param station
     * @return 
     */
    private static List<Integer> getZones(String station) {
        List<Integer> zones = new ArrayList<>();
        if (ZONE_1.contains(station)) {
            zones.add(1);
        }
        if (ZONE_2.contains(station)) {
            zones.add(2);
        }
        if (ZONE_3.contains(station)) {
            zones.add(3);
        }
        if (ZONE_4.contains(station)) {
            zones.add(4);
        }
        return zones;
    }

}
